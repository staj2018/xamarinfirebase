﻿using System;
using Android.App;
using Android.Content;
using ChatApp.Droid;
using ChatApp.Main;
using Firebase.Auth;
using Xamarin.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GoogleLoginPage), typeof(LoginPageRenderer))]

namespace ChatApp.Droid
{
    public class LoginPageRenderer : PageRenderer
    {
        public static OAuth2Authenticator Auth;

        public LoginPageRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);

            var activity = Context as Activity;

            Auth = new OAuth2Authenticator(
                clientId: "471944628355-406uhg0r5e2naqke24soacmc9gvq4qoo.apps.googleusercontent.com",
                clientSecret: null,
                scope: "openid",
                authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"),
                redirectUrl: new Uri("com.googleusercontent.apps.471944628355-406uhg0r5e2naqke24soacmc9gvq4qoo:/oauth2redirect"),
                accessTokenUrl: new Uri("https://www.googleapis.com/oauth2/v4/token"),
                getUsernameAsync: null,
                isUsingNativeUI: true
            );
            activity.StartActivity(Auth.GetUI(activity));
            
            Auth.Completed += (sender, eventArgs) =>
            {
                if (eventArgs.IsAuthenticated)
                {
                    App.Instance.User = new Main.User{AuthType = FirebaseAuthType.Google, Token = eventArgs.Account.Properties["access_token"]};
                    MessagingCenter.Send<Object>(this, "Authenticated");
                }
            };
            CustomTabsConfiguration.CustomTabsClosingMessage = null;
        }
    }
}