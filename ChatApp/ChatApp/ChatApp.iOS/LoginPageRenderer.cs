﻿using System;
using ChatApp.iOS;
using ChatApp.Main;
using Firebase.Auth;
using Xamarin.Auth;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(GoogleLoginPage), typeof(LoginPageRenderer))]

namespace ChatApp.iOS
{
    public class LoginPageRenderer : PageRenderer
    {

        bool IsShown;
        public static OAuth2Authenticator Auth;

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (!IsShown)
            {

                IsShown = true;


                Auth = new OAuth2Authenticator(
                    clientId: "471944628355-406uhg0r5e2naqke24soacmc9gvq4qoo.apps.googleusercontent.com",
                    clientSecret: null,
                    scope: "openid",
                    authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"),
                    redirectUrl: new Uri("com.googleusercontent.apps.471944628355-406uhg0r5e2naqke24soacmc9gvq4qoo:/oauth2redirect"),
                    accessTokenUrl: new Uri("https://www.googleapis.com/oauth2/v4/token"),
                    getUsernameAsync: null,
                    isUsingNativeUI: true
                );

                Auth.Completed += (sender, eventArgs) =>
                {
                    if (eventArgs.IsAuthenticated)
                    {
                        App.Instance.User = new Main.User { AuthType = FirebaseAuthType.Google, Token = eventArgs.Account.Properties["access_token"] };
                    }
                };

                PresentViewController(Auth.GetUI(), true, null);

            }

        }
    }
}