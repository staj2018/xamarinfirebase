﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using ChatApp.Main;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;

namespace ChatApp.Database
{
    class Db
    {
        public FirebaseAuthProvider pro;
        public FirebaseClient client;
        private string CONFIG_KEY = "AIzaSyABXy8mIPFwNi28gSYzcxYotj7gheihSuk";

        public Db()
        {
            pro = new FirebaseAuthProvider(new FirebaseConfig(CONFIG_KEY));
            client = new FirebaseClient("https://xamarin-firebase-2018.firebaseio.com/");
        }

        public async Task<List<Friend>> getFriendsList()
        {
            var FriendList = new List<Friend>();
            foreach (var j in await client.Child("Users").Child(App.Instance.User.Key).Child("Friends").OnceAsync<Friend>())
                FriendList.Add(j.Object);
            return FriendList;
        }

        public async Task<List<Friend>> getChatsList()
        {
            var chats = new List<Friend>();
            foreach (var j in await client.Child("Users").Child(App.Instance.User.Key).Child("Friends").OnceAsync<Friend>())
            {
                if(j.Object.RoomKey!=null)
                    chats.Add(j.Object);
            }

            return chats;
        }

        public async Task<List<Friend>> searchUser(string Name)
        {
            var UserList = new List<Friend>();
            var Friendlist = await getFriendsList();
            foreach (var i in await client.Child("Users").OnceAsync<FirebaseUser>())
            {
                if (i.Object.Name.ToLower().StartsWith(Name) || i.Object.Name.StartsWith(Name))
                {
                    bool boo = true;
                    foreach (var j in Friendlist)
                    {
                        if (i.Key.Equals(j.Key))
                        {
                            boo = false;
                            break;
                        }
                    }

                    if (i.Object.LocalId.Equals(App.Instance.User.LocalId)) boo = false;
                    if (boo) UserList.Add(new Friend{Key=i.Key, Name=i.Object.Name, Photo = i.Object.Photo});
                }
            }

            return UserList;
        }

        public async Task<List<Friend>> searchFriend(string Name)
        {
            var FriendList = new List<Friend>();
            foreach (var i in await client.Child("Users").Child(App.Instance.User.Key).Child("Friends").OnceAsync<Friend>())
            {
                if (i.Object.Name.ToLower().StartsWith(Name) || i.Object.Name.StartsWith(Name))
                {
                    if (!i.Object.Key.Equals(App.Instance.User.Key)) FriendList.Add(i.Object);
                }
            }
            return FriendList;
        }

        public async void addFriend(Friend friend)
        {
            await client.Child("Users").Child(App.Instance.User.Key).Child("Friends").PostAsync(friend);
            bool boo = true;
            foreach (var i in await client.Child("Users").Child(App.Instance.User.Key).Child("Requests").OnceAsync<Friend>())
            {
                if (i.Object.Key.Equals(friend.Key))
                {
                    await client.Child("Users").Child(App.Instance.User.Key).Child("Requests").Child(i.Key).DeleteAsync();
                    boo = false;
                }
            }

            if (boo)
            {
                await client.Child("Users").Child(friend.Key).Child("Requests").PostAsync(new Friend{Key = App.Instance.User.Key, Name = App.Instance.User.Name, Photo = App.Instance.User.Photo });
            }
        }

        public async Task<List<Chat>> getChat(string roomKey)
        {
            List<Chat> chatList = new List<Chat>();
            foreach (var i in await client.Child("Rooms").Child(roomKey).OnceAsync<Chat>())
                chatList.Add(i.Object);
            return chatList;
        }

        public async Task<List<Friend>> getNot()
        {
            List<Friend> notList = new List<Friend>();
            foreach (var i in await client.Child("Users").Child(App.Instance.User.Key).Child("Requests").OnceAsync<Friend>())
                notList.Add(i.Object);
            return notList;
        }
    }
}
