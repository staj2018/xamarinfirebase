﻿using System;
using System.Diagnostics;
using ChatApp.Main;
using Firebase.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using User = ChatApp.Main.User;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]

namespace ChatApp
{

    //SHA1
    //C:\Program Files\Java\jre1.8.0_171\bin>keytool -exportcert -alias androiddebugkey -keystore "C:\Users\defne.tuncer\AppData\Local\Xamarin\Mono for Android\debug.keystore" -list -v

    //INCASE OF EMERGENCY DELETE
    //C:\Users\defne.tuncer\Desktop\xamarinfirebase\ChatApp\ChatApp\ChatApp.Android\obj\Debug
    //C:\Users\defne.tuncer\AppData\Local\Xamarin

    public partial class App
    {
        static volatile App _Instance;
        static object _SyncRoot = new Object();
        public User User;

        public App()
        {
            MainPage = new HomePage();
        }

        public static App Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (_SyncRoot)
                    {
                        if (_Instance == null)
                        {
                            _Instance = new App();
                        }
                    }
                }
                return _Instance;
            }
        }

        public bool IsAuthenticated
        {
            get { return User!=null; }
        }
    }
}
