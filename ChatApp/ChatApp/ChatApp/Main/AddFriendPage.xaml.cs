﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChatApp.Database;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using Xamarin.Forms;

namespace ChatApp.Main
{
    public partial class AddFriendPage : ContentPage
    {
        Db db = new Db();

        public AddFriendPage()
        {
            InitializeComponent();
        }

        async void UserList_OnUserSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var selectedUser = ((Friend)UserList.SelectedItem);
            var answer = await DisplayAlert("", "You're now connected with " + selectedUser.Name, "OK", "CANCEL");
            if (answer)
            {
                db.addFriend(selectedUser);
                UserList.BeginRefresh();
            }
        }

        async void Name_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (Name.Text != "") UserList.BindingContext = await db.searchUser(Name.Text);
        }

        async void UserList_OnRefreshing(object sender, EventArgs e)
        {
            if (Name.Text != null) UserList.BindingContext = await db.searchUser(Name.Text);
            UserList.IsRefreshing = false;
        }
    }
}
