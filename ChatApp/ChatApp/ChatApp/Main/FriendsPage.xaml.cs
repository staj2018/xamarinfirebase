﻿using ChatApp.Database;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChatApp.Main
{
    public partial class FriendsPage : ContentPage
    {
        Db db = new Db();

        public FriendsPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (home.IsVisible)
           {
               home.IsVisible = false;
               friends.IsVisible = false;
               chat.IsVisible = false;
               exit.IsVisible = false;
            }

            FriendList.BindingContext = await db.getFriendsList();

        }

        async void OnFriendSelected(object sender, SelectedItemChangedEventArgs args)
        {
            await Navigation.PushModalAsync(new Room((Friend)FriendList.SelectedItem));
        }

        async void AddFriend_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new AddFriendPage());
        }

        private async void FriendList_OnRefreshing(object sender, EventArgs e)
        {
            FriendList.BindingContext = await db.getFriendsList();
            FriendList.IsRefreshing = false;
        }

        async void Name_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (Name.Text != "") FriendList.BindingContext = await db.searchFriend(Name.Text);
        }

        private async void Menu_Button_OnClicked(object sender, EventArgs e)
        {
            if (home.IsVisible)
            {
                await Task.WhenAll(home.TranslateTo(0, -App.Current.MainPage.Height/2),
                    friends.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    chat.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    exit.TranslateTo(0, -App.Current.MainPage.Height / 2));
                home.IsVisible = false;
                friends.IsVisible = false;
                chat.IsVisible = false;
                exit.IsVisible = false;
                await Task.WhenAll(home.TranslateTo(0, 0),
                    friends.TranslateTo(0, 0),
                    chat.TranslateTo(0, 0),
                    exit.TranslateTo(0, 0));
            }
            else
            {
                await Task.WhenAll(home.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    friends.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    chat.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    exit.TranslateTo(0, -App.Current.MainPage.Height / 2));
                home.IsVisible = true;
                friends.IsVisible = true;
                chat.IsVisible = true;
                exit.IsVisible = true;
                await Task.WhenAll(home.TranslateTo(0, 0),
                    friends.TranslateTo(0, 0),
                    chat.TranslateTo(0, 0),
                    exit.TranslateTo(0, 0));
            }
        }

        private void Home_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new HomePage());
        }

        private void Chat_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ChatPage());
        }

        private void Friends_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new FriendsPage());
        }

        private void Out_Button_OnClicked(object sender, EventArgs e)
        {
            App.Instance.User = null;
            Navigation.PushModalAsync(new MainPage());
        }
    }
}
