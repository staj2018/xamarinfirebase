﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChatApp.Database;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using Xamarin.Forms;

namespace ChatApp.Main
{
    public partial class Room : ContentPage
    {
        Db db = new Db();
        private Friend friend;

        public Room()
        {
            InitializeComponent();
        }

        public Room(Friend friend)
        {
            this.friend = friend;
            _name.Text = friend.Name;
            _photo.Source = friend.Photo;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            message.BindingContext = "";
            if (friend.RoomKey != null)
                _chat.BindingContext = await db.getChat(friend.RoomKey);
        }

        async void Handle_Clicked(object sender, EventArgs e)
        {

            if (message.Text != "")
            {
                var chat = new Chat {Message = message.Text, Key = App.Instance.User.Key};
                var messageText = message.Text;
                message.Text = "";

                if (friend.RoomKey == null)
                {
                    var room = await db.client.Child("Rooms").PostAsync(new FirebaseRoom());
                    friend.RoomKey = room.Key;
                    await db.client.Child("Rooms").Child(friend.RoomKey).PostAsync(chat);
                    _chat.BindingContext = await db.getChat(friend.RoomKey);

                    //update Users -> App.Instance.User -> Friends -> Friend.RoomKey
                    foreach (var i in await db.client.Child("Users").Child(App.Instance.User.Key).Child("Friends").OnceAsync<Friend>())
                    {
                        if (i.Object.Key.Equals(friend.Key))
                        {
                            var Friend = new Friend { Key = friend.Key, Name = friend.Name, Photo = friend.Photo, RoomKey = room.Key, LastMessage = messageText};
                            await db.client.Child("Users").Child(App.Instance.User.Key).Child("Friends").Child(i.Key).DeleteAsync();
                            await db.client.Child("Users").Child(App.Instance.User.Key).Child("Friends").PostAsync(Friend);
                        }
                    }

                    bool boo = true;
                    var self = new Friend { Key = App.Instance.User.Key, Name = App.Instance.User.Name, Photo = App.Instance.User.Photo, RoomKey = room.Key, LastMessage = messageText};
                    
                    //if friends -> update Users -> friend -> Friends -> App.Instance.User.RoomKey
                    foreach (var i in await db.client.Child("Users").Child(friend.Key).Child("Friends").OnceAsync<Friend>())
                    {
                        if (i.Object.Key.Equals(App.Instance.User.Key))
                        {
                            await db.client.Child("Users").Child(friend.Key).Child("Friends").Child(i.Key).DeleteAsync();
                            await db.client.Child("Users").Child(friend.Key).Child("Friends").PostAsync(self);
                            boo = false;
                            break;
                        }
                    }

                    //if not friends -> update Users -> friend -> Requests -> App.Instance.User.RoomKey
                    if (boo)
                    {
                        foreach (var i in await db.client.Child("Users").Child(friend.Key).Child("Requests").OnceAsync<Friend>())
                        {
                            if (i.Object.Key.Equals(App.Instance.User.Key))
                            {
                                await db.client.Child("Users").Child(friend.Key).Child("Requests").Child(i.Key).DeleteAsync();
                                await db.client.Child("Users").Child(friend.Key).Child("Requests").PostAsync(self);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    await db.client.Child("Rooms").Child(friend.RoomKey).PostAsync(chat);
                    _chat.BindingContext = await db.getChat(friend.RoomKey);

                    //update last message
                    foreach (var i in await db.client.Child("Users").Child(App.Instance.User.Key).Child("Friends").OnceAsync<Friend>())
                    {
                        if (i.Object.Key.Equals(friend.Key))
                        {
                            var Friend = new Friend { Key = friend.Key, Name = friend.Name, Photo = friend.Photo, RoomKey = friend.RoomKey, LastMessage = messageText };
                            await db.client.Child("Users").Child(App.Instance.User.Key).Child("Friends").Child(i.Key).DeleteAsync();
                            await db.client.Child("Users").Child(App.Instance.User.Key).Child("Friends").PostAsync(Friend);
                            break;
                        }
                    }

                    //update last message
                    foreach (var i in await db.client.Child("Users").Child(friend.Key).Child("Friends").OnceAsync<Friend>())
                    {
                        if (i.Object.Key.Equals(App.Instance.User.Key))
                        {
                            var self = new Friend { Key = App.Instance.User.Key, Name = App.Instance.User.Name, Photo = App.Instance.User.Photo, RoomKey = friend.RoomKey, LastMessage = messageText };
                            await db.client.Child("Users").Child(friend.Key).Child("Friends").Child(i.Key).DeleteAsync();
                            await db.client.Child("Users").Child(friend.Key).Child("Friends").PostAsync(self);
                            break;
                        }
                    }
                }
            }
        }
    }
}

