﻿using System;
using System.Collections.Generic;
using System.Text;
using ChatApp.Database;
using Firebase.Database.Query;

namespace ChatApp.Main
{
    public class Friend
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public string LastMessage { get; set; }
        public string RoomKey { get; set; }
    }
}
