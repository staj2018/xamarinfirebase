﻿using Xamarin.Forms;

namespace ChatApp.Main
{
    class ChatTemplateSelector : DataTemplateSelector
    {
        DataTemplate incomingDataTemplate;
        DataTemplate outgoingDataTemplate;

        public ChatTemplateSelector()
        {
            this.incomingDataTemplate = new DataTemplate(typeof(IncomingViewCell));
            this.outgoingDataTemplate = new DataTemplate(typeof(OutgoingViewCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var messageVm = item as Chat;
            if (messageVm == null)
                return null;


            return (messageVm.Key == App.Instance.User.Key) ? outgoingDataTemplate : incomingDataTemplate;
        }

    }
}
