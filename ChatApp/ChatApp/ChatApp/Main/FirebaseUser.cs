﻿namespace ChatApp.Main
{
    public class FirebaseUser
    {
        public string LocalId { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
    }
}
