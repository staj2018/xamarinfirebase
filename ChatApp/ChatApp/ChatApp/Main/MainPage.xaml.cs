﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ChatApp.Database;
using Firebase.Auth;
using Firebase.Database.Query;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Firebase.Database;

namespace ChatApp.Main
{
    public partial class MainPage : ContentPage
    {
        Db db = new Db();
        public MainPage()
        {
           InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (signup.IsVisible)
                signup.IsVisible = false;
        }

        private async void Signup_OnClicked(object sender, EventArgs e)
        {
            await signup.TranslateTo(Application.Current.MainPage.Width, 0);
            await main.TranslateTo(-Application.Current.MainPage.Width, 0);
            signup.IsVisible = true;
            await signup.TranslateTo(0, 0);
        }

        private async void Signup_Back_OnClicked(object sender, EventArgs e)
        {
            await signup.TranslateTo(Application.Current.MainPage.Width, 0);
            await main.TranslateTo(0, 0);
            signup.IsVisible = false;
            await signup.TranslateTo(0, 0);
        }

        private async void Login_Email_OnClicked(object sender, EventArgs e)
        {
            if (password.Text == null || email.Text == null)
            {
                await DisplayAlert("", "Please fill all fields.", "OK");
            }
            else
            {
                try
                {
                    var auth = await db.pro.SignInWithEmailAndPasswordAsync(email.Text, password.Text);
                    var client = new FirebaseClient("https://xamarin-firebase-2018.firebaseio.com/",
                        new FirebaseOptions
                        {
                            AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken)
                        });
                    foreach (var i in await client.Child("Users").OnceAsync<FirebaseUser>())
                    {
                        if (i.Object.LocalId.Equals(auth.User.LocalId))
                        {
                            App.Instance.User = new User { Name = i.Object.Name, AuthType = FirebaseAuthType.EmailAndPassword, Key = i.Key, LocalId = i.Object.LocalId, Photo = i.Object.Photo };
                            break;
                        }
                    }
                    await Navigation.PushModalAsync(new HomePage());
                }
                catch (FirebaseAuthException)
                {
                    await DisplayAlert("", "The email or password that you have entered is incorrect.", "OK");
                }
            }
        }

        private async void Signup_Email_OnClicked(object sender, EventArgs e)
        {
            if (password_signup.Text == null || password2_signup.Text == null || email_signup.Text == null || name_signup.Text == null)
            {
                await DisplayAlert("", "Please fill all fields.", "OK");
            }
            else if (!password_signup.Text.Equals(password2_signup.Text))
            {
                await DisplayAlert("", "Passwords do not match.", "OK");
            }
            else if (password_signup.Text.Length < 6)
            {
                await DisplayAlert("", "Password should be at least 6 characters.", "OK");
            }
            else
            {
                try
                {
                    var auth = await db.pro.CreateUserWithEmailAndPasswordAsync(email_signup.Text,
                        password_signup.Text);
                    var client = new FirebaseClient("https://xamarin-firebase-2018.firebaseio.com/",
                        new FirebaseOptions
                        {
                            AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken)
                        });
                    var photo = (new Random().Next(1, 3) == 1) ? "rick.png" : "morty.png";
                    var post = await client.Child("Users").PostAsync(new FirebaseUser { Name = name_signup.Text, Photo = photo, LocalId = auth.User.LocalId });
                    App.Instance.User = new User { Name = name_signup.Text, Photo = photo, LocalId = auth.User.LocalId, AuthType = FirebaseAuthType.EmailAndPassword, Key = post.Key };
                    await Navigation.PushModalAsync(new HomePage());
                }
                catch (FirebaseAuthException)
                {
                    await DisplayAlert("", "You already have an account.", "OK");
                }
            }
        }

        private void Google_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new GoogleLoginPage());
            Navigation.PushModalAsync(new HomePage());
        }

        private void Facebook_OnClicked(object sender, EventArgs e)
        {

        }

        protected override bool OnBackButtonPressed()
        {
            if (signup.IsVisible)
                Signup_Back_OnClicked(null, EventArgs.Empty);
            else
                return false;
            return true;

        }


        /*
        private void Login_Facebook_OnClicked(object sender, EventArgs e)
        {

            var loginUri = this.GenerateFacebookLoginUrl(FacebookAppId, "email");

            this.Browser.Visibility = Visibility.Visible;
            this.Browser.Navigate(loginUri);

        }

        private Uri GenerateFacebookLoginUrl(string appId, string extendedPermissions)
        {

            dynamic parameters = new ExpandoObject();
            parameters.client_id = appId;
            parameters.redirect_uri = "https://www.facebook.com/connect/login_success.html";

            // The requested response: an access token (token), an authorization code (code), or both (code token).
            parameters.response_type = "token";

            // list of additional display modes can be found at http://developers.facebook.com/docs/reference/dialogs/#display
            parameters.display = "popup";

            // add the 'scope' parameter only if we have extendedPermissions.
            if (!string.IsNullOrWhiteSpace(extendedPermissions))
                parameters.scope = extendedPermissions;

            // generate the login url
            var fb = new FacebookClient();
            return fb.GetLoginUrl(parameters);
        }

        private void BrowserNavigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            var fb = new FacebookClient();
            FacebookOAuthResult oauthResult;
            if (!fb.TryParseOAuthCallbackUrl(e.Uri, out oauthResult))
            {
                return;
            }

            if (oauthResult.IsSuccess)
            {
                this.Browser.Visibility = Visibility.Collapsed;
                this.FetchFirebaseData(oauthResult.AccessToken, FirebaseAuthType.Facebook);
            }
        }

    */

    }
}
