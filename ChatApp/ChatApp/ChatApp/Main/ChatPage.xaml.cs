﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChatApp.Database;
using Firebase.Database;
using Firebase.Database.Query;
using Xamarin.Forms;

namespace ChatApp.Main
{
    public partial class ChatPage : ContentPage
    {

        Db db = new Db();

        public ChatPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (home.IsVisible)
            {
               home.IsVisible = false;
               friends.IsVisible = false;
               chat.IsVisible = false;
               exit.IsVisible = false;
            }
            ChatList.BindingContext = await db.getChatsList();
        }

        private async void Menu_Button_OnClicked(object sender, EventArgs e)
        {
            if (home.IsVisible)
            {
                await Task.WhenAll(home.TranslateTo(0, -App.Current.MainPage.Height/2),
                    friends.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    chat.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    exit.TranslateTo(0, -App.Current.MainPage.Height / 2));
                home.IsVisible = false;
                friends.IsVisible = false;
                chat.IsVisible = false;
                exit.IsVisible = false;
                await Task.WhenAll(home.TranslateTo(0, 0),
                    friends.TranslateTo(0, 0),
                    chat.TranslateTo(0, 0),
                    exit.TranslateTo(0, 0));
            }
            else
            {
                await Task.WhenAll(home.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    friends.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    chat.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    exit.TranslateTo(0, -App.Current.MainPage.Height / 2));
                home.IsVisible = true;
                friends.IsVisible = true;
                chat.IsVisible = true;
                exit.IsVisible = true;
                await Task.WhenAll(home.TranslateTo(0, 0),
                    friends.TranslateTo(0, 0),
                    chat.TranslateTo(0, 0),
                    exit.TranslateTo(0, 0));
            }
        }

        private void Home_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new HomePage());
        }

        private void Chat_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ChatPage());
        }

        private void Friends_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new FriendsPage());
        }

        private void Out_Button_OnClicked(object sender, EventArgs e)
        {
            App.Instance.User = null;
            Navigation.PushModalAsync(new MainPage());
        }

        private async void ChatList_OnRefreshing(object sender, EventArgs e)
        {
            ChatList.BindingContext = await db.getChatsList();
            ChatList.IsRefreshing = false;
        }

        private void ChatList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Navigation.PushModalAsync(new Room((Friend)ChatList.SelectedItem));
        }
    }
}
