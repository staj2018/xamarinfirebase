﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChatApp.Database;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using Xamarin.Forms;

namespace ChatApp.Main
{
    public partial class HomePage : ContentPage
    {

        Db db = new Db();

        public HomePage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (!App.Instance.IsAuthenticated)
            {
                await Navigation.PushModalAsync(new MainPage());
            }
            else
            {
                if (home.IsVisible)
                {
                    home.IsVisible = false;
                    friends.IsVisible = false;
                    chat.IsVisible = false;
                    exit.IsVisible = false;
                }
                if (!App.Instance.User.AuthType.Equals(FirebaseAuthType.EmailAndPassword))
                    FetchFirebaseData();
                else
                {
                    name.Text = App.Instance.User.Name;
                    photo.Source = App.Instance.User.Photo;
                }
                Notifications.BindingContext = await db.getNot();
            }
        }

        private async void FetchFirebaseData()
        {
            Db db = new Db();
            var auth = await db.pro.SignInWithOAuthAsync(App.Instance.User.AuthType, App.Instance.User.Token);

            name.Text = auth.User.DisplayName;
            photo.Source = auth.User.PhotoUrl;

            var client = new FirebaseClient("https://xamarin-firebase-2018.firebaseio.com/",
                new FirebaseOptions { AuthTokenAsyncFactory = () => Task.FromResult(auth.FirebaseToken) });

            bool boo = true;
            foreach (var i in await client.Child("Users").OnceAsync<User>())
            {
                if (i.Object.LocalId.Equals(auth.User.LocalId))
                {
                    App.Instance.User = new User {Key = i.Key, Name = i.Object.Name, AuthType = App.Instance.User.AuthType, LocalId = i.Object.LocalId, Photo = i.Object.Photo};
                    boo = false;
                    break;
                }
            }

            if (boo)
            {
                var post = await client.Child("Users").PostAsync(new FirebaseUser { LocalId = auth.User.LocalId, Name = auth.User.DisplayName, Photo = auth.User.PhotoUrl });
                App.Instance.User = new User { Key = post.Key, Name = auth.User.DisplayName, AuthType = App.Instance.User.AuthType, LocalId = auth.User.LocalId, Photo = auth.User.PhotoUrl };
            }
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        private async void Menu_Button_OnClicked(object sender, EventArgs e)
        {
            if (home.IsVisible)
            {
                await Task.WhenAll(home.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    friends.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    chat.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    exit.TranslateTo(0, -App.Current.MainPage.Height / 2));
                home.IsVisible = false;
                friends.IsVisible = false;
                chat.IsVisible = false;
                exit.IsVisible = false;
                await Task.WhenAll(home.TranslateTo(0, 0),
                    friends.TranslateTo(0, 0),
                    chat.TranslateTo(0, 0),
                    exit.TranslateTo(0, 0));
            }
            else
            {
                await Task.WhenAll(home.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    friends.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    chat.TranslateTo(0, -App.Current.MainPage.Height / 2),
                    exit.TranslateTo(0, -App.Current.MainPage.Height / 2));
                home.IsVisible = true;
                friends.IsVisible = true;
                chat.IsVisible = true;
                exit.IsVisible = true;
                await Task.WhenAll(home.TranslateTo(0, 0),
                    friends.TranslateTo(0, 0),
                    chat.TranslateTo(0, 0),
                    exit.TranslateTo(0, 0));
            }
        }

        private void Home_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new HomePage());
        }

        private void Chat_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ChatPage());
        }

        private void Friends_Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new FriendsPage());
        }

        private void Out_Button_OnClicked(object sender, EventArgs e)
        {
            App.Instance.User = null;
            Navigation.PushModalAsync(new MainPage());
        }

        private async void Notifications_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedItem = (Friend) Notifications.SelectedItem;
            var answer = await DisplayAlert("","Add "+selectedItem.Name + " as Friend?", "OK", "CANCEL");
            if (answer)
            {
                db.addFriend(selectedItem);
                Notifications.BeginRefresh();
            }
        }

        private async void Notifications_OnRefreshing(object sender, EventArgs e)
        {
            Notifications.BindingContext = await db.getNot();
            Notifications.IsRefreshing = false;

        }
    }
}