﻿using System.Collections.Generic;
using Firebase.Auth;

namespace ChatApp.Main
{
    public class User
    {
        public string LocalId { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public string Key { get; set; }
        public string Token { get; set; }
        public FirebaseAuthType AuthType { get; set; }
    }
}
